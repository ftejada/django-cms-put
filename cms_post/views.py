from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt # Excepción para la seguridad, CSRF es
# Create your views here.

from .models import Contenido

formulario = """
<form action="" method="POST">
    <p>valor: <input type="text" name="valor"></p>
    <p><input type="submit" value="Enviar"></p>
</form>
"""

@csrf_exempt # Decorador para quiatr el control de seguridad de CSRF
def get_resources(request, recurso):

    # Métodos PUT-POST
    if request.method in ["PUT","POST"]:
        if request.method == "POST":
            Valor = request.POST["valor"]
        else:
            Valor = request.body.decode() # Campo que se rellena en el REST


        try:
            contenido = Contenido.objects.get(clave=recurso)  # Consultamos en la BBDD si tenemos un recurso ya pedido
            contenido.valor = Valor
            contenido.save()
        except Contenido.DoesNotExist:
            c=Contenido(clave=recurso, valor=Valor) # instanciar el contenido
            c.save() # La clave es el recurso (arriba)


    # Gestionar métodos GET:
    try:
        contenido = Contenido.objects.get(clave=recurso) # Consultamos en la BBDD si tenemos un recurso ya pedido
        response = HttpResponse("El recurso es: "+contenido.clave + ", cuyo valor es " +contenido.valor+ " y su identificador es " +str(contenido.id))
    except Contenido.DoesNotExist:
        response = HttpResponse(formulario)

    return response

