from django.db import models
# Create your models here.

class Contenido (models.Model):

    clave = models.CharField(max_length=64) # Cadena de caracteres de máximo 64 bytes
    valor = models.TextField() # Contenido de la página HTML sin límite de caracteres

    def __str__(self):
        return str(self.id)+": "+self.clave

    # Inserción de comentarios
class Comentario (models.Model):

    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=128)
    comentario = models.TextField()
    fecha = models.DateTimeField()

    def __str__(self):
        return str(self.id) +": "+self.titulo+" --- "+self.contenido.clave

    def verificar(self):
        return ("cuerpo" in self.comentario)


