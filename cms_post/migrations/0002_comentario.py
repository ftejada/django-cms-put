# Generated by Django 4.1.7 on 2023-03-23 12:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("cms_post", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Comentario",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("titulo", models.CharField(max_length=128)),
                ("comentario", models.TextField()),
                ("fecha", models.DateTimeField()),
                (
                    "contenido",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="cms_post.contenido",
                    ),
                ),
            ],
        ),
    ]
